#include <boost/python/module.hpp>
#include <boost/python.hpp>

#include "DRSReader.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(DRSReaderModule){
  numpy::initialize();
  class_<DRSReader,boost::noncopyable>("DRSReader",init<>())
    .def("FindDRS",&DRSReader::FindDRS)
    .def("SetFrequency",&DRSReader::SetFrequency)
    .def("GetNominalFrequency",&DRSReader::GetNominalFrequency)
    .def("GetTrueFrequency",&DRSReader::GetTrueFrequency)
    .def("EnableTcal",&DRSReader::EnableTcal)
    .def("EnableAcal",&DRSReader::EnableAcal)
    .def("SetTranspMode",&DRSReader::SetTranspMode)
    .def("SetChannelConfig",&DRSReader::SetChannelConfig)
    .def("SetInputRange",&DRSReader::SetInputRange)
    .def("GetInputRange",&DRSReader::GetInputRange)
    .def("EnableTrigger",&DRSReader::EnableTrigger)
    .def("AddTriggerSourceOr",&DRSReader::AddTriggerSourceOr)
    .def("AddTriggerSourceAnd",&DRSReader::AddTriggerSourceAnd)
    .def("RemoveTriggerSourceOr",&DRSReader::RemoveTriggerSourceOr)
    .def("RemoveTriggerSourceAnd",&DRSReader::RemoveTriggerSourceAnd)
    .def("GetTriggerSource",&DRSReader::GetTriggerSource)
    .def("SetTriggerLevel",&DRSReader::SetTriggerLevel)
    .def("SetTriggerPolarity",&DRSReader::SetTriggerPolarity)
    .def("SetTriggerDelayNs",&DRSReader::SetTriggerDelayNs)
    .def("SetUpTrigger",&DRSReader::SetUpTrigger)
    .def("SoftTrigger",&DRSReader::SoftTrigger)
    .def("GetCalibratedFrequency",&DRSReader::GetCalibratedFrequency)
    .def("GetCalibratedInputRange",&DRSReader::GetCalibratedInputRange)
    .def("IsTimingCalibrationValid",&DRSReader::IsTimingCalibrationValid)
    .def("IsVoltageCalibrationValid",&DRSReader::IsVoltageCalibrationValid)
    .def("CalibrateVolt",&DRSReader::CalibrateVolt)
    .def("CalibrateTiming",&DRSReader::CalibrateTiming)
    .def("StartDomino",&DRSReader::StartDomino)
    .def("IsBusy",&DRSReader::IsBusy)
    .def("IsEventAvailable",&DRSReader::IsEventAvailable)
    .def("TransferWaves",&DRSReader::TransferWaves)
    .def("TransferWaves",&DRSReader::TransferWaves1)
    .def("TransferWaves",&DRSReader::TransferWaves2)
    .def("GetWaveform",&DRSReader::pyGetWaveform)
  ;
}
