#include <boost/python/numpy.hpp>
#include <iostream>

#include "DRSReader.h"

using namespace std;

namespace p = boost::python;
namespace np = boost::python::numpy;

class ProgressBar : public DRSCallback
{
public:
   void Progress(int prog);
};

void ProgressBar::Progress(int prog)
{
   if (prog == 0)
      printf("[--------------------------------------------------]\r");
   printf("[");
   for (int i=0 ; i<prog/2 ; i++)
      printf("=");
   printf("\r");
   fflush(stdout);
}

DRSReader::DRSReader(){};

DRSReader::~DRSReader(){
  delete drs;
}

//Scans for DRS boards
//Intializes the first DRS
int DRSReader::FindDRS(){
  drs = new DRS();

  int nBoards = drs->GetNumberOfBoards();
  cout << nBoards;
  if(nBoards==1)cout << " DRS board found" << endl;
  else cout << " DRS boards found" << endl;

  b = drs->GetBoard(0);

  //cout << "DRS Type: " << b->GetDRSType() << endl;
  //cout << "Board Type: " << b->GetBoardType() << endl;
  b->Init();
  return nBoards;
};

void DRSReader::SetTranspMode(int mode){b->SetTranspMode(mode);}

void DRSReader::SetInputRange(float center){b->SetInputRange(center);}
float DRSReader::GetInputRange(){return b->GetInputRange();}

void DRSReader::SetChannelConfig(int firstChannel,int lastChannel,int nConfigChannels){b->SetChannelConfig(firstChannel,lastChannel,nConfigChannels);};

void DRSReader::SetFrequency(double freq,bool wait){b->SetFrequency(freq,wait);}
float DRSReader::GetNominalFrequency(){return b->GetNominalFrequency();}
float DRSReader::GetTrueFrequency(){return b->GetTrueFrequency();}

void DRSReader::EnableTcal(int freq, int level, int phase){b->EnableTcal(freq,level,phase);}
void DRSReader::EnableAcal(int mode, double voltage){b->EnableAcal(mode,voltage);}

void DRSReader::EnableTrigger(int flag1, int flag2){b->EnableTrigger(flag1, flag2);}

/*
  The DRS4 allows to set an OR trigger logic
  and an AND trigger logic independently.
  The following functions allow to add and remove
  channels from this logic.
  ch 0...3 refer to the analog inputs.
  ch 4 refers to the TRIGGER IN.
*/
void DRSReader::AddTriggerSourceOr(int ch){
  TriggerSource |= (1<<ch);
  b->SetTriggerSource(TriggerSource);
};

void DRSReader::RemoveTriggerSourceOr(int ch){
  TriggerSource &= ~(1<<ch);
  b->SetTriggerSource(TriggerSource);
};

void DRSReader::AddTriggerSourceAnd(int ch){
  TriggerSource |= (1<<(ch+8));
  b->SetTriggerSource(TriggerSource);
};

void DRSReader::RemoveTriggerSourceAnd(int ch){
  TriggerSource &= ~(1<<(ch+8));
  b->SetTriggerSource(TriggerSource);
};

int DRSReader::GetTriggerSource(){
  unsigned int trig = b->GetTriggerSource();
  return trig;
};

void DRSReader::SetTriggerLevel(float level){b->SetTriggerLevel(level);}

//rising  -> pol = false
//falling -> pol = true
void DRSReader::SetTriggerPolarity(bool pol){b->SetTriggerPolarity(pol);}
//Sets the trigger delay in ns.
void DRSReader::SetTriggerDelayNs(float time){b->SetTriggerDelayNs(time);}
void DRSReader::SoftTrigger(){b->SoftTrigger();}

bool DRSReader::IsTimingCalibrationValid(){return b->IsTimingCalibrationValid();};
bool DRSReader::IsVoltageCalibrationValid(){return b->IsVoltageCalibrationValid();};

//This allows to calibrate the voltages of the adc.
void DRSReader::CalibrateVolt(){
  cout << "Please disconnect all inputs and press enter." << endl;
  cin.get();
  ProgressBar p;
  b->SetRefclk(0);
  //cout << "Frequency: " << b->GetNominalFrequency() << endl;
  b->SetFrequency(b->GetNominalFrequency(),true);
  //cout << "Range: " << b->GetInputRange() << endl;
  b->SetInputRange(b->GetInputRange());
  b->CalibrateVolt(&p);
  cout << endl;
}

//This allows to calibrate the timing of each bin
void DRSReader::CalibrateTiming(){
  ProgressBar p;
  b->SetFrequency(b->GetNominalFrequency(),true);
  b->CalibrateTiming(&p);
  cout << endl;
}

double DRSReader::GetCalibratedInputRange(){return b->GetCalibratedInputRange();}

double DRSReader::GetCalibratedFrequency(){return b->GetCalibratedFrequency();}

void DRSReader::StartDomino(){b->StartDomino();}

bool DRSReader::IsEventAvailable(){return b->IsEventAvailable();};

bool DRSReader::IsBusy(){return b->IsBusy();}

int DRSReader::TransferWaves(int firstChannel,int lastChannel){return b->TransferWaves(firstChannel,lastChannel);}
//Two convenience functions for the python wrapper
int DRSReader::TransferWaves1(int lastChannel){return b->TransferWaves(0,lastChannel);}
int DRSReader::TransferWaves2(){return b->TransferWaves(0,8);}

int DRSReader::GetWaveform(int channel,float* time,float* amplitude){
  int status = 0;
  status = b->GetTime(0,channel*2,b->GetNominalFrequency(),b->GetTriggerCell(0),time);//,true,true);
  status+= b->GetWave(0,channel*2,amplitude,true,b->GetTriggerCell(0),b->GetStopWSR(0),false,0,true);
  return status;
}

void DRSReader::SetUpTrigger(bool polarity,float level,float delay,bool enable_flag1,bool enable_flag2){
  SetTranspMode(1);
  SetTriggerPolarity(polarity);
  SetTriggerLevel(level);
  SetTriggerDelayNs(delay);
  EnableTrigger(enable_flag1,enable_flag2);
}

//Convenience function for the python wrapper
//returning a numpy array
np::ndarray DRSReader::pyGetWaveform(int channel,const int nbins){
  float time[nbins];
  float amplitude[nbins];
  DRSReader::GetWaveform(channel,time,amplitude);

  p::tuple shape = p::make_tuple(2,nbins);
  np::dtype dtype = np::dtype::get_builtin<float>();
  np::ndarray waveform = np::zeros(shape,dtype);
  for(int i=0;i<nbins;i++){
    //cout << time[i] << "\t" << amplitude[i] << endl;
    waveform[0][i] = time[i];
    waveform[1][i] = amplitude[i];
  }
  return waveform;
}

DRSBoard* DRSReader::GetDRSBoard(){return b;}
