#include <iostream>

#include "DRSReader.h"

using namespace std;

int main(){
  DRSReader* reader = new DRSReader();
  reader->FindDRS();
  reader->SetFrequency(0.7,true);
  reader->SetTranspMode(1);
  //reader->EnableTcal(1);
  reader->SetInputRange(0.0);
  reader->EnableTrigger(1,0);
  reader->SetTriggerDelayNs(500);
  reader->AddTriggerSourceOr(0);
  // reader->RemoveTriggerSourceOr(1);
  // reader->RemoveTriggerSourceOr(2);
  // reader->RemoveTriggerSourceOr(3);
  // reader->RemoveTriggerSourceOr(4);
  //reader->AddTriggerSourceAnd(0);
  //reader->AddTriggerSourceAnd(1);
  //cout << "Trigger source: " << reader->GetTriggerSource() << endl;
  reader->SetTriggerLevel(0.1);

  cout << "Timing: " << reader->IsTimingCalibrationValid() << endl;
  cout << "Voltage: " << reader->IsVoltageCalibrationValid() << endl;

  //reader->CalibrateTiming();
  //cout << "\nCalibrated Timing" << endl;
  //reader->CalibrateVolt();
  //cout << "\nCalibrated Voltage" << endl;

  //reader->EnableTcal(1);
  //reader->EnableAcal(1);

  reader->StartDomino();
  // int available = reader->IsEventAvailable();
  // while(!available){
  //   cout << "Event available: " << available << endl;
  //   available = reader->IsEventAvailable();
  // }
  bool busy = reader->IsBusy();
  while(busy){
    cout << "IsBusy: " << busy << endl;
    //reader->SoftTrigger();
    busy = reader->IsBusy();
  }
  cout << "IsBusy: " << busy << endl;
  reader->TransferWaves(0,8);

  float time[1024];
  float amplitude[1024];
  int status = reader->GetWaveform(0,time,amplitude);
  cout << status << endl;
  for(int i=0;i<1024;i++){
    cout << time[i] << "\t" << amplitude[i] << endl;
  }

  delete reader;
}
