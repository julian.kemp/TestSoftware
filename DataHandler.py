from __future__ import print_function

import os
import numpy as np

import ROOT as r

class DataHandler:
    def __init__(self,filename):
        """ Class that handles the data taken by the SSD test stand.

        Keyword arguments:
        filename    -- Name of the root file where the data should be stored
        """
        if filename[-5:]!='.root':
            filename += '.root'
        while os.path.exists(filename):
            inp = raw_input("File " + filename + " already exists. Overwrite? [y,N] ")
            if inp.upper()=='Y':
                break
            else:
                filename = raw_input("Please insert new filename: ")
        self.outFile = r.TFile(filename,'RECREATE','SSD Test Result',9)
        if self.outFile.IsZombie():
            print("Error file "+filename+" could not be opened.")
            sys.exit(1)
        self.dataTree = r.TTree('data','Tree of event data') #Tree to store events triggered by MiniAMD
        self.softTree = r.TTree('soft','Tree of soft triggered data') #Tree to store soft triggered events

        self.dataTime = np.zeros(1,dtype=float)
        self.softTime = np.zeros(1,dtype=float)
        self.dataTrace = r.TH1F()
        self.softTrace = r.TH1F()
        self.SignalHistogram = r.TH1F("SignalHistogram","Histogram of Signal Charges",1000,-1e7,1e8)
        self.SoftHistogram = r.TH1F("SoftHistogram","Histogram of Soft Triggered Charges",1000,-1e7,1e8)

        self.dataTree.Branch('Time',self.dataTime,'Time/D')
        self.dataTree.Branch('Trace','TH1F',self.dataTrace)
        self.softTree.Branch('Time',self.softTime,'Time/D')
        self.softTree.Branch('Trace','TH1F',self.softTrace)

    def __del__(self):
        if self.outFile.IsOpen():
            self.outFile.Close()

    def AddTrace(self,unixtime,timing,amplitude,soft=False,fill_histogram=True,millivolt_to_electrons=1e7/50/1.602,baseline_window=[1,320],signal_window=[330,450],bandwidth_limit=60):
        """ Adds a trace to the data tree.

        Keyword arguments:
        unixtime                -- Unixtime of the event in seconds
        timing                  -- Array of the times of the trace
        amplitude               -- Array of the amplitudes of the trace
        soft                    -- Bool, stating if event is soft triggered (default is False)
        fill_histogram          -- Bool, stating if event integral should be added to the histogram (default is True)
        millivolt_to_electrons  -- Conversion factor from milli volt amplitude to charge in number of electrons.
                                   Default is for 1GHz sampling and 50Ohms input impedance.
        baseline_window         -- Tuple with low and high index for determination of baseline
        signal_window           -- Tuple with low and high index for integration of signal
        bandwidth_limit         -- Cutoff frequency for the waveform in MHz. Will be applied through FFT. Set to 0 if no limit shall be applied.
        """

        timing = np.append(timing,[timing[-1]+timing[-1]-timing[-2]]) #Make one bin edge more than number of samples
        timing = timing.astype(float)
        print(timing)

        if soft:
            self.softTime[0] = unixtime
            self.softTrace.SetBins(len(timing)-1,timing)
            for i,amp in enumerate(amplitude):
                self.softTrace.SetBinContent(i+1,amp)
            self.softTree.Fill()
        else:
            self.dataTime[0] = unixtime
            self.dataTrace.SetBins(len(timing)-1,timing)
            for i,amp in enumerate(amplitude):
                self.dataTrace.SetBinContent(i+1,amp)
            self.dataTree.Fill()

        if fill_histogram:
            if bandwidth_limit>0: # Limit the bandwidth by performin a fft
                dt = np.mean((timing[1:]-timing[:-1])*1e-9) # bin width in seconds
                fftamplitude = np.fft.rfft(amplitude)
                fftfrequency = np.fft.rfftfreq(len(amplitude),dt)
                cutoff_bin = np.searchsorted(fftfrequency,bandwidth_limit*1e6)
                fftamplitude[cutoff_bin:] = 0 # Set all contributions abpve the cutoff_bin to 0
                amplitude = np.fft.irfft(fftamplitude)
            baseline = np.median(amplitude[baseline_window[0]:baseline_window[1]])
            signal_charge = -np.sum(amplitude[signal_window[0]:signal_window[1]]-baseline)
            if soft:
                self.SoftHistogram.Fill(millivolt_to_electrons*signal_charge)
            else:
                self.SignalHistogram.Fill(millivolt_to_electrons*signal_charge)

    def WriteTrees(self):
        """ Writes the trees containing the traces to the file """
        self.outFile.cd()
        self.dataTree.Write()
        self.softTree.Write()

    def WriteHistograms(self):
        """ Writes the Signal and Soft Trigger histograms to the file """
        self.outFile.cd()
        self.SignalHistogram.Write()
        self.SoftHistogram.Write()

    def CloseFile(self):
        """ Closes the root TFile where the data has been written to """
        self.outFile.Close()
