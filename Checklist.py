from __future__ import print_function

def ChecklistStart():
    """ Runs a checklist before startinpg the SSD tests """

    inp = raw_input("1.\tDid you connect the grounding cable to SSD? [y,N] ")
    if inp.upper()!='Y':
        print('Please connect the grounding cable to SSD.')
        return 1

    inp = raw_input("2.\tDid you check that MiniAMD is placed at the correct position and height? [y,N]" +
                    "\n\t(Black labels for the height (mind the parallax!), about 39cm to each side of the detector) ")
    if inp.upper()!='Y':
        print('Please move MiniAMD to the correct position and height.')
        return 1

    inp = raw_input("3.\tDid you connect the MiniAMD power plug? [y,N] ")
    if inp.upper()!='Y':
        print('Please connect the MiniAMD power plug.')
        return 1

    inp = raw_input("4.\tDid you connect the MiniAMD ethernet cable? [y,N] ")
    if inp.upper()!='Y':
        print('Please connect the MiniAMD ethernet cable.')
        return 1

    inp = raw_input("5.\tDid you connect the MiniAMD trigger cable to MiniAMD and the DRS4? [y,N] ")
    if inp.upper()!='Y':
        print('Please connect the MiniAMD trigger cable to MiniAMD and the DRS4.')
        return 1

    inp = raw_input("6.\tDid you connect the PMT signal cable? [y,N] ")
    if inp.upper()!='Y':
        print('Please connect the PMT signal cable.')
        return 1

    inp = raw_input("7.\tDid you connect the PMT HV cable? [y,N] ")
    if inp.upper()!='Y':
        print('Please connect the PMT HV cable.')
        return 1

    inp = raw_input("8.\tDid you set the PMT HV to 1400V? [y,N] ")
    if inp.upper()!='Y':
        print('Please slowly ramp up the PMT HV to 1400V.')
        return 1

    return 0

def ChecklistEnd():
    inp = ''
    while not inp.upper()=='Y':
        inp = raw_input('1.\tIs the MiniAMD SiPM voltage ramped down? [y,N] ')
        if inp.upper()!='Y':
            print('Please ramp down the MiniAMD SiPM voltage.')
            print('Open a new terminal and execute "python RunSSDTest.py --sipms=off".')
            raw_input('Press enter to continue...')

    inp = ''
    while not inp.upper()=='Y':
        inp = raw_input('2.\tDid the MiniAMD Odroid shutdown? [y,N] ')
        if inp.upper()!='Y':
            print('Please shut down the MiniAMD Odroid.')
            print('Open a new terminal and execute "python RunSSDTest.py --shutdown".')
            raw_input('Press enter to continue...')

    inp = ''
    while not inp.upper()=='Y':
        inp = raw_input('3.\tDid you ramp down the PMT HV? [y,N] ')
        if inp.upper()!='Y':
            print('Please slowly ramp down the PMT HV.')
            raw_input('Press enter to continue...')

    return 0
