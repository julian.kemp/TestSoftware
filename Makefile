DOS=OS_LINUX
CXX=g++

CFLAGS=-g -O2 -Wall -Wuninitialized -fno-strict-aliasing -Iinclude -I/usr/local/include -D$(DOS) -DHAVE_USB -DHAVE_LIBUSB10 -DUSE_DRS_MUTEX
LIBS=-lpthread -lutil -lusb-1.0
WXLIBS=$(shell wx-config --libs)

DRS_LIB_DIR=$(HOME)/Documents/Uni/Promotion/Measurements/SSDTest/DRS4/drs-5.0.6/
PYTHON_LIB_DIR=/usr/lib64/python3.6m
BOOST_LIB_DIR=/usr/lib64

DRS_INC_DIR=$(HOME)/Documents/Uni/Promotion/Measurements/SSDTest/DRS4/drs-5.0.6/include
PYTHON_INC_DIR=/usr/include/python3.6m
BOOST_INC_DIR=/usr/include

OBJECTS=$(DRS_LIB_DIR)/musbstd.o $(DRS_LIB_DIR)/mxml.o $(DRS_LIB_DIR)/strlcpy.o $(DRS_LIB_DIR)/averager.o

all: main DRSReaderModule.so

main: DRSReader.o main.o
	$(CXX) $(CFLAGS) $(OBJECTS) DRSReader.o main.o $(DRS_LIB_DIR)/DRS.o -o main $(LIBS) $(WXLIBS) -L$(BOOST_LIB_DIR) -lboost_python-py3 -lboost_numpy-py3 -L$(PYTHON_LIB_DIR)/config -lpython3.6m

DRSReaderModule.so: DRSReader.o DRSReaderModule.o
	$(CXX) -shared -Wl,--export-dynamic DRSReaderModule.o DRSReader.o $(OBJECTS) $(DRS_LIB_DIR)/DRS.o -L$(BOOST_LIB_DIR) -lboost_python-py3 -lboost_numpy-py3 -L$(PYTHON_LIB_DIR)/config -lpython3.6m $(LIBS) $(WXLIBS) -std=c++11 -o DRSReaderModule.so

DRSReaderModule.o: DRSReaderModule.cpp DRSReader.o DRSReader.cpp DRSReader.h
	$(CXX) -I$(PYTHON_INC_DIR) -I$(BOOST_INC_DIR) -I$(DRS_INC_DIR) -fPIC -std=c++11 -c DRSReaderModule.cpp

DRSReader.o: DRSReader.cpp DRSReader.h
	$(CXX) $(CFLAGS) -I$(DRS_INC_DIR) -I$(BOOST_INC_DIR) -I$(PYTHON_INC_DIR) -fPIC -c $<

main.o: main.cpp
	$(CXX) $(CFLAGS) -I$(DRS_INC_DIR) -I$(BOOST_INC_DIR) -I$(PYTHON_INC_DIR) -c $<
