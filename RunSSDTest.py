###########################################################
# This software allows to run the SSD test procedure.     #
#                                                         #
# author: Julian Kemp                                     #
# e-mail: kemp@physik.rwth-aachen.de                      #
# date: 22.06.2018                                        #
#                                                         #
############################################################

import signal
import sys
import getopt
import numpy as np
import time

import MiniAMD
from DRSReaderModule import DRSReader
import Checklist
import DataHandler

terminate = False
def signal_handling(signum,frame):
    global terminate
    terminate = True

signal.signal(signal.SIGINT,signal_handling)

def getFloatInput(message):
    while True:
        try:
            ret = float(input(message))
        except:
            print("Wrong format! Please insert a float!")
            continue
        break
    return ret

def help():
    print("RunSSDTest.py [OPTIONS]")
    print("\nPossible [OPTIONS]:")
    print("\t-h [--help]\t\tPrint this help")
    print("\t-c [--calibrate=]\tShould be followed by 'volt' or 'time'\n\t\t\t\tCalibrates the DRS4 voltage/timing")
    print("\t-s [--sipms=]\t\tOnly option is 'OFF'. Turns MiniAMD SiPMs off.")
    print("\t-r [--run]\t\tRun the SSD Test procedure.")
    print("\t-v [--voltage=]\t\tSets the center of the DRS4 input voltage range in V.")
    print("\t-f [--frequency=]\tSets the DRS4 sampling frequency in GHz.")
    print("\t--starttrigger\t\tWill start the MiniAMD trigger independently of the other settings.")
    print("\t--stoptrigger\t\tWill stop the MiniAMD trigger smoothely if it is running.")
    print("\t--killtrigger\t\tKills the MiniAMD trigger process. Attention, the SiPM voltage is not ramped down in this case!")
    print("\t--triggeror=\t\tSpecify a channel [0..4] for the trigger OR logic, 4 is the external trigger input.")
    print("\t--triggerand=\t\tSpecify a channel [0..4] for the trigger AND logic, 4 is the external trigger input.")
    print("\t--triggerdelay=\t\tSpecify the trigger delay in ns (default is 300)")
    print("\t--nevents=\t\tMaximum number of events to trigger on.")
    print("\t--timeout=\t\tMaximum time to wait for a new trigger in s. After specified time DAQ will stop and all events stored to file.")
    print("\t--softperiod=\t\tPeriod in s for the soft trigger to save events.")
    print("\t--hostname=\t\tHostname to connect to MiniAMD")
    print("\t--serialnumber=\t\tSerial number of the SSD. It can be found near the PMT tube.")
    print("\t\t\t\tPlease specify it in the Form '02AAXXXX_side', where side is either 'left' or 'right'.")
    print("\t--bandwidthlimit=\tSpecify the limit for the bandwidth in MHz to be applied to the waveform through an FFT.")
    print("\t--shutdown\t\tShutdown the MiniAMD Odroid.")


#Variables to parse the command line arguments
doVoltageCalibration = False
doTimingCalibration = False
runTest = False
checklist = True
nEvents = np.inf #Number of events to record
timeout = np.inf #Maximum time to wait for a new trigger
soft_period = np.inf
serial_number = '02AA0000'
baseline_window = [1,520]
signal_window = [530,650]
bandwidth_limit = 60 # limit for the bandwidth in MHz, will be applied through FFT

#Variables for MiniAMD
SiPMState = 'none'
hostname = 'lx3aauger03'
starttrigger = False
stoptrigger = False
killtrigger = False
shutdown = False

#Variables for the DRS4
voltageSpecified = False
voltage = 0.0
frequencySpecified = False
frequency = 0.96 #8x the sampling frequency of the UUB
trigger_source_and = []
trigger_source_or = []
trigger_delay = 300
trigger_polarity = True #True = falling, False = rising
trigger_level = 0.0
trigger_enable_flag1 = True
trigger_enable_flag2 = False

try:
    opts,args = getopt.getopt(sys.argv[1:],"hc:s:rv:f:",
                              ["help","calibrate=","sipms=","run",
                               "voltage=","frequency=","starttrigger",
                               "nochecklist","triggeror=","triggerand=","nevents=",
                               "timeout=","softperiod=","hostname=","serialnumber=",
                               "stoptrigger","killtrigger","triggerdelay=",
                               "bandwidthlimit="])
except getopt.GetoptError:
    help()
    sys.exit(2)

#Check all the arguments
for opt,arg in opts:
    if opt in ('-h','--help'):
        help()
        sys.exit()
    elif opt in ('-c','--calibrate'):
        if arg=='volt':
            doVoltageCalibration = True
        elif arg=='time':
            doTimingCalibration = True
        else:
            help()
            sys.exit(2)
    elif opt in ('-v','--voltage'):
        voltageSpecified = True
        voltage = float(arg)
        if voltage <= -0.5 or voltage > 0.5:
            print('Voltage not in the allowed range of (-0.5,0.5] V')
            help()
            sys.exit(2)
    elif opt in ('-f','--frequency'):
        frequencySpecified = True
        frequency = float(arg)
        if frequency < 0.7 or frequency > 5:
            print(opt+'\tFrequency not in the allowed range of [0.7,5] GHz')
            help()
            sys.exit(2)
    elif opt in ('-r','--run'):
        runTest = True
    elif opt in ('-s','--sipms'):
        if arg.upper()=='OFF':
            SiPMState=arg.upper()
        else:
            help()
            sys.exit(2)
    elif opt=='--starttrigger':
        starttrigger = True
    elif opt=='--nochecklist':
        checklist = False
    elif opt=='--triggeror':
        try:
            ch = int(arg)
        except:
            print(opt+'\tTrigger channel must be an integer')
            sys.exit(2)
        if ch<0 or ch>4:
            print(opt+'\tTrigger channel must be [0..4]')
            sys.exit(2)
        trigger_source_or.append(ch)
    elif opt=='--triggerand':
        try:
            ch = int(arg)
        except:
            print(opt+'\tTrigger channel must be an integer')
            sys.exit(2)
        if ch<0 or ch>4:
            print(opt+'\tTrigger channel must be [0..4]')
            sys.exit(2)
        trigger_source_and.append(ch)
    elif opt=='--nevents':
        try:
            nEvents = int(arg)
        except:
            print(opt+'\tNumber of events must be an integer.')
    elif opt=='--timeout':
        try:
            timeout = int(arg)
        except:
            print(opt+'\tTimeout must be an integer.')
    elif opt=='--triggerdelay':
        try:
            trigger_delay = int(arg)
        except:
            print(opt+'\tTrigger delay must be an integer.')
    elif opt=='--softperiod':
        try:
            soft_period = float(arg)
        except:
            print(opt+'\tPeriod of soft trigger must be an integer.')
    elif opt=='--bandwidthlimit':
        try:
            bandwidth_limit = int(arg)
        except:
            print(opt+'\tBandwidth limit must be an integer.')
    elif opt=='--hostname':
        hostname = arg
    elif opt=='--serialnumber':
        serial_number = arg
    elif opt=='--stoptrigger':
        stoptrigger = True
    elif opt=='--killtrigger':
        killtrigger = True
    elif opt=='--shutdown':
        shutdown = True

# Conversion from integrated voltage trace in millivolt to number of electrons.
# Conversion is done for 50 Ohms termination.
millivolt_to_electrons = 1e-3*1e-9/frequency/50./1.602/1e-19

# Starting and stopping/killing the trigger does not make sense
if (starttrigger and stoptrigger) or (starttrigger and killtrigger):
    print("ERROR: You cannot start and stop/kill the trigger at the same time!")
    sys.exit(2)

# Starting the trigger and shutting down MiniAMD does not make sense
if shutdown and starttrigger:
    print("ERROR: You cannot start the MiniAMD trigger and shutdown MiniAMD.")
    sys.exit(2)

# If no trigger channel has been parsed through the command line,
# set external trigger as default trigger channel
if len(trigger_source_and)==0 and len(trigger_source_or)==0:
    trigger_source_and.append(4)

if runTest and serial_number=='02AA0000':
    print("Attention! A test will be run but no serialnumber was given.")
    inp = ''
    while inp.upper()!='P' and inp.upper()!='S' and inp.upper()!='E':
        inp = input('Do you want to proceed (p), enter a serialnumber (s) or exit (e)? ')
    if inp.upper()=='S':
        serial_number = input('Please specify the serial number in the form "02AAXXXX_side". ')
    elif inp.upper()=='E':
        sys.exit(0)

#When the SSD test is run the checklist should be done first.
if runTest and checklist:
    if Checklist.ChecklistStart():
        sys.exit(2)

#Only initialize the DRS4 if it is really needed
if doVoltageCalibration or doTimingCalibration or runTest:
    drs = DRSReader()
    drs.FindDRS()

#Only initialize miniAMD if it is really needed
if SiPMState!='none' or runTest or starttrigger or killtrigger or stoptrigger or shutdown:
    amd = MiniAMD.MiniAMD(hostname=hostname)
    try:
        amd.connect()
    except:
        print('ERROR: Cannot connect to MiniAMD. Please make sure that MiniAMD is switched on, all ethernet cables are connected and the correct hostname is given.')
        sys.exit(2)

if stoptrigger:
    amd.StopTrigger()

if killtrigger:
    amd.KillTrigger()

if shutdown:
    amd.shutdown()

if SiPMState=='OFF':
    print("Switching MiniAMD SiPM voltage off")
    amd.SwitchSiPMVoltageOff()

if starttrigger:
    print("Starting MiniAMD trigger.")
    if amd.StartTrigger()==0:
        print('MiniAMD trigger started successfully.')
    else:
        print('ERROR: MiniAMD trigger did not start.')
        sys.exit(2)

if doVoltageCalibration:
    print("Performing voltage calibration of the DRS4")
    if not voltageSpecified:
        while True:
            voltage = getFloatInput('Specify the center of the input range (-0.5,0.5] ')
            if voltage > -0.5 and voltage <= 0.5:
                break
            else:
                print('Voltage out of allowed range!')
    drs.SetInputRange(voltage)
    drs.StartDomino()
    print('Started DRS4 domino. Waiting 30 seconds for it to warm up...')
    time.sleep(30)
    drs.CalibrateVolt()
    print("Voltage calibration finished. You can now reconnect all signal cables.")
    input("Hit enter to continue")

if doTimingCalibration:
    print("Performing timing calibration of the DRS4")
    if not frequencySpecified:
        while True:
            frequency = getFloatInput('Specify the frequency in GHz [0.7,5] ')
            if frequency >= 0.7 and frequency <= 5.:
                break
            else:
                print('Frequency not in the allowed range')
    drs.SetFrequency(frequency,True)
    drs.CalibrateTiming()
    print("Timing calibration finished.")

# Run the Test procedure
if runTest:
    print("="*50)
    print(" "*10+"Starting the SSD test procedure"+" ")
    print("="*50)

    print("\n\n")
    print("="*50)
    print(" "*15+"Setting up the DRS4"+" "*15)
    print("="*50)
    print("\n\n")

    #Setup DRS4 frequency and voltage
    drs.SetFrequency(frequency,True)
    drs.SetInputRange(voltage)

    #Check if DRS4 timing calibration is valid for this frequency
    print("The DRS4 frequency is set to {:.2f} GHz.".format(drs.GetNominalFrequency()))
    print("The DRS4 true frequency is {:.2f} GHz.".format(drs.GetTrueFrequency()))
    if drs.GetNominalFrequency()==drs.GetCalibratedFrequency():
        print("The DRS4 timing calibration is valid.")
    else:
        print("ERROR: The DRS4 was timing calibrated at {:.2f} GHz".format(drs.GetCalibratedFrequency()))
        print("ERROR: The timing calibration for the DRS4 is not valid!")
        sys.exit(2)

    #Check if DRS4 voltage calibration is valid for this input range
    print("The DRS4 input range is set to [{:.1f},{:.1f}] V.".format(drs.GetInputRange()-0.5,drs.GetInputRange()+0.5))
    if drs.GetCalibratedInputRange()==drs.GetInputRange():
        print("The DRS4 voltage calibration is valid.")
    else:
        print("ERROR: The DRS4 was calibrated for the range [{:.1f},{:.1f}] V".format(drs.GetCalibratedInputRange()-0.5,drs.GetCalibratedInputRange()+0.5))
        print("ERROR: The voltage calibration for the DRS4 is not valid!")
        sys.exit(2)

    print("The DRS4 trigger is set up.")
    for source in trigger_source_or:
        drs.AddTriggerSourceOr(source)
    for source in trigger_source_and:
        drs.AddTriggerSourceAnd(source)

    drs.SetUpTrigger(trigger_polarity,trigger_level,trigger_delay,trigger_enable_flag1,trigger_enable_flag2)

    print("\n\n")
    print("="*50)
    print(" "*14+"Setting up the MiniAMD"+" "*14)
    print("="*50)
    print("\n\n")

    #Setup the MiniAMD
    if amd.StartTrigger()==0:
        print('MiniAMD trigger started successfully.')
    else:
        print('ERROR: MiniAMD trigger did not start.')
        sys.exit(2)

    handler = DataHandler.DataHandler('Test_'+serial_number)

    soft_start = time.time() # Start time for soft trigger period

    while not terminate:
        drs.StartDomino()
        start = time.time()
        timeout_exceeded = False
        soft_triggered = False
        while drs.IsBusy():
            if time.time()-start>timeout: # Check if timeout is exceeded
                timeout_exceeded = True
                break
            if time.time()-soft_start>soft_period: # Check if time for soft trigger is reached
                drs.SoftTrigger()
                soft_triggered = True
                soft_start = time.time()
        now = time.time()
        if timeout_exceeded:
            print("ERROR: Timeout exceeded! No trigger within last {:d} s.".format(timeout))
            break
        drs.TransferWaves(0)
        trace = drs.GetWaveform(0,1024)
        print(soft_triggered)
        handler.AddTrace(now,trace[0],trace[1],soft_triggered,millivolt_to_electrons=millivolt_to_electrons,baseline_window=baseline_window,signal_window=signal_window,bandwidth_limit=bandwidth_limit)

    handler.WriteTrees()
    handler.WriteHistograms()
    handler.CloseFile()

    print("\n\n")
    print("="*50)
    print(" "*14+"Shutting down MiniAMD"+" "*14)
    print("="*50)
    print("\n\n")

    if amd.StopTrigger()==0:
        amd.shutdown()
    else:
        inp = input("Do you want to kill the process? [y,N] ")
        if inp.upper()=='Y':
            amd.KillTrigger()
            inp = input("Do you want to switch off the SiPM voltage? [y,N] ")
            if inp.upper()=='Y':
                amd.SwitchSiPMVoltageOff()

    if checklist:
        while ChecklistEnd(): pass
