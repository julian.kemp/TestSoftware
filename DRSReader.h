/********************************************************************\

  Name:         DRSReader.h
  Created by:   Julian Kemp

  Contents:     Convenience class to read data from DRS4

\********************************************************************/

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include "DRS.h"

namespace np = boost::python::numpy;

class DRSReader: boost::noncopyable {
  public:
    DRSReader();
    ~DRSReader();
    int FindDRS();
    void SetFrequency(double freq,bool wait);
    float GetNominalFrequency();
    float GetTrueFrequency();
    void EnableTcal(int freq, int level=0, int phase=0);
    void EnableAcal(int mode, double voltage=0);
    void SetChannelConfig(int firstChannel,int lastChannel, int nConfigChannels);

    void SetTranspMode(int mode);

    void SetInputRange(float center);
    float GetInputRange();

    //flag1 for external trigger
    //flag2 for analog threshold trigger
    void EnableTrigger(int flag1, int flag2);

    //Add and Remove Trigger sources
    //ch=[0..3] = analog inputs
    //ch=4 = external trigger source
    void AddTriggerSourceOr(int ch);
    void AddTriggerSourceAnd(int ch);
    void RemoveTriggerSourceOr(int ch);
    void RemoveTriggerSourceAnd(int ch);
    int GetTriggerSource();

    void SetTriggerLevel(float level);
    void SetTriggerPolarity(bool pol); //pol=false for rising edge
    void SetTriggerDelayNs(float time); //Trigger delay in ns
    void SetUpTrigger(bool polarity,float level,float delay,bool enable_flag1,bool enable_flag2);

    void SoftTrigger();

    bool IsTimingCalibrationValid();
    bool IsVoltageCalibrationValid();

    void CalibrateVolt();
    void CalibrateTiming();

    double GetCalibratedInputRange();
    double GetCalibratedFrequency();

    void StartDomino();
    bool IsEventAvailable();
    bool IsBusy();

    int TransferWaves(int firstChannel=0,int lastChannel=8);
    int TransferWaves1(int lastChannel);
    int TransferWaves2();
    int GetWaveform(int channel,float* time,float* amplitude);
    np::ndarray pyGetWaveform(int channel,const int nbins);

    DRSBoard* GetDRSBoard();

  private:
    DRS* drs;
    DRSBoard* b;
    unsigned int TriggerSource = 0;
};
